import asyncio
import json
import re
import time
from datetime import datetime

import deepl
import emoji
import numpy as np
import requests
import unicodedata
from googletrans import Translator
from httpx._config import Timeout
from telethon import TelegramClient
from telethon.tl.types import ReactionEmoji

# telegram api credentials https://my.telegram.org/
api_id = ...
api_hash = ...

# deepl api credentials https://www.deepl.com/docs-api/api-access/authentication
deepl_auth_key = ...

# libre translate mirror
libre_tr_url = "https://translate.terraprint.co/translate"


def format_message(text):
    text = unicodedata.normalize('NFKD', text)

    # remove special characters
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               u"\U00002500-\U00002BEF"  # chinese char
                               u"\U00002702-\U000027B0"
                               u"\U000024C2-\U0001F251"
                               u"\U0001f926-\U0001f937"
                               u"\U00010000-\U0010ffff"
                               u"\u2640-\u2642"
                               u"\u2600-\u2B55"
                               u"\u200d"
                               u"\u23cf"
                               u"\u23e9"
                               u"\u231a"
                               u"\ufe0f"  # dingbats
                               u"\u3030"
                               "]+", re.UNICODE)
    text = emoji_pattern.sub(r'', text)

    # remove links
    text = re.sub(r'\(?https?://\w*\)?', '', text, flags=re.MULTILINE)

    # remove repeating newlines
    text = re.sub(r'\n+', '\n', text, flags=re.MULTILINE)

    return text


def format_ru(text):
    # remove promo
    text = re.sub(r'Подписаться на СМИ', '', text)

    return text


def format_ukr(text):
    # remove promo
    text = re.sub(r'ТРУХА⚡️Украина | Прислать новость', '', text)

    return text


def process_reactions(reactions):
    result = {}

    if reactions is None or reactions.results is None:
        return result

    for r in reactions.results:
        if isinstance(r.reaction, ReactionEmoji):
            result[emoji.demojize(r.reaction.emoticon)] = (r.reaction.emoticon, r.count)

    return result


# translate messages to English using DeepL
def translate(text, target_lang='EN-US'):
    translator = deepl.Translator(deepl_auth_key)
    result = translator.translate_text(text, target_lang=target_lang)
    return result.text


# translate messages to English using LibreTranslate
def translate_free(text, source_lang='ru', target_lang='en'):
    response = requests.post(
        libre_tr_url,
        headers={
            "Content-Type": "application/json"
        },
        json={
            'q': text,
            'source': source_lang,
            'target': target_lang,
            'format': 'text'
        }
    )
    if response.status_code != 200:
        print(f'Error translating text: {response.status_code}')
        raise Exception(response.text)

    return response.json()['translatedText']


def translate_google(text, source_lang='ru', target_lang='en'):
    time.sleep(1)
    return Translator(timeout=Timeout(60)).translate(text, src=source_lang, dest=target_lang).text


async def get_messages(chat, msg_limit, session='anon', translate_fn=translate_free, format_fn=format_ru,
                       source_lang='ru', file='messages_ru.json'):
    start_time = time.time()
    avg_time_d = []
    result = []

    date_format = "%m/%d/%Y, %H:%M:%S"
    msg_limit_date = datetime.strptime("01/01/2023, 00:00:00", date_format)

    async with TelegramClient(session, api_id, api_hash) as client:
        async for msg in client.iter_messages(chat):

            if msg.message is None:
                continue

            # handle messages after "9/1/2023 00:00:00"
            if msg.date.replace(tzinfo=None) < msg_limit_date:
                return result

            time_d = time.time()

            # process message
            msg_text = format_fn(format_message(msg.message))

            # skip short messages
            if len(msg_text) < 300:
                continue

            if msg_text != '':
                result.append({
                    'id': msg.id,
                    'timestamp': msg.date.strftime(date_format),
                    'reactions': process_reactions(msg.reactions),
                    'text': translate_fn(msg_text, source_lang=source_lang),
                })

                # calculate estimated time remaining
                time_d = time.time() - time_d
                avg_time_d.append(time_d)

                hours, remainder = divmod(time.time() - start_time, 3600)
                minutes, seconds = divmod(remainder, 60)

                hours_r, remainder_r = divmod((msg_limit - len(result)) * np.mean(avg_time_d), 3600)
                minutes_r, seconds_r = divmod(remainder_r, 60)

                print(f'{len(result):>3} out of {msg_limit} - '
                      f'running for {hours:02.0f}:{minutes:02.0f}:{seconds:02.0f}, '
                      f'time left {hours_r:02.0f}:{minutes_r:02.0f}:{seconds_r:02.0f}')

            if len(result) == msg_limit:
                return result

            if len(result) % 50 == 0:
                # serialize to json file
                with open(file, 'w', encoding='utf-8') as f:
                    json.dump(result, f, ensure_ascii=False, indent=4)


def get_messages_ru(msg_limit=5):
    url_ru = 'https://t.me/novosti_voinaa'

    # get n recent messages
    messages = asyncio.run(
        get_messages(url_ru, session='anon1', msg_limit=msg_limit, format_fn=format_ru, translate_fn=translate_google,
                     source_lang='ru', file='messages_ru.json'))


def get_messages_ukr(msg_limit=5):
    url_ukr = 'https://t.me/truexanewsua'

    # get n recent messages
    asyncio.run(
        get_messages(url_ukr, session='anon', msg_limit=msg_limit, format_fn=format_ukr, translate_fn=translate_google,
                     source_lang='uk', file='messages_ukr.json'))


if __name__ == '__main__':
    get_messages_ukr(msg_limit=5000)
