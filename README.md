# mvi-sp

## Data
`message_scraper.py` contains the data gathering pipeline (missing telegram, deepl auth keys)

`messages_ukr.json` 1445 recent messages from "Труха⚡️Україна" telegram channel

`messages_ukr.json` 842 recent messages from "СМИ Россия не Москва" telegram channel

## KG, LLMs, RAG application 
`kg_builder.ipynb` Google Colab notebook with brief showcase of KG construction and RAG application

`kg_builder_details.ipynb` Google Colab notebook with detailed framework walk-through

`test_results_kro.png` online test results